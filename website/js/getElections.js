function updateElections(elections) {
    const electionsElement = document.getElementById('elections');
    electionsElement.innerHTML = "";

    for (let name in elections) {
        if (!elections.hasOwnProperty(name)) continue;

        const formID = name.toLowerCase().replaceAll(' ', '-');

        const form = $(`<form id="${formID}" name="${name}" onsubmit="castVote(this); return false;"></form>`);
        for (let runnerObject of elections[name]) {

            const runnerBtn = $('<button class="collapsible" form="none"></button>').text(runnerObject.name);

            const speech = $('<p></p>').text(runnerObject.speech);
            const speechContainer = $('<div class="content"></div>');
            speechContainer.append(speech);

            const info = $('<div class="info"></div>');
            info.append(runnerBtn, speechContainer);

            const voteBtn = $(`<input type="radio" name="runner" value="${runnerObject.name}" form="${formID}" required/>`);
            const vote = $('<div class="vote"></div>');
            vote.append(voteBtn);

            const runner = $('<div class="runner"></div>');
            runner.append(vote, info);
            form.append(runner);
        }

        const electionName = $('<div class="election-name"></div>').text(name);
        const election = $('<div class="election"></div>');

        const castVote = $('<div class="cast-vote"></div>');
        const castVoteBtn = $(`<button type="submit" form="${formID}"></button>`).text("Cast Vote");
        castVote.append(castVoteBtn);
        form.append(castVote);

        election.append(electionName, form);
        $(electionsElement).append(election);
    }
    const coll = document.getElementsByClassName("collapsible");
    for (let i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            this.classList.toggle("active");
            const content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
}

window.addEventListener("load", async () => {
    let result = await $.ajax({
        url: API_URL + "get-current-election",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json",
        dataType: "json"
    });
    updateElections(result);
});