function loginSuccess(account) {

    const signInButton = document.getElementsByClassName('account-button')[0];
    signInButton.innerHTML = `Sign out of<br>${account}`;
    signInButton.style.fontSize = "100%"
    const elections = document.getElementById('elections');
    elections.style.display = 'flex';
    const signInNotice = document.getElementById('sign-in-notice');
    signInNotice.style.display = 'none';
    signInButton.setAttribute("onclick", "signOut();");
}

function logOutSuccess() {
    const signInButton = document.getElementsByClassName('account-button')[0];
    signInButton.innerHTML = "Sign In";
    signInButton.style.fontSize = "200%";
    const elections = document.getElementById('elections');
    elections.style.display = 'none';
    const signInNotice = document.getElementById('sign-in-notice');
    signInNotice.style.display = 'inherit';
    signInButton.setAttribute("onclick", "signIn();");
}

async function castVote(form) {
    const formData = $(form).serializeArray();

    async function sendVote(data, endpoint) {
        const voteData = {
            position_name: form.name,
            elector_name: formData[0].value,
            voter: {
                id: data.id,
                job: data.jobTitle,
                email: data.mail,
                office: data.officeLocation
            }
        }
        let success = await $.ajax({
            url: API_URL + "cast-vote",
            type: "POST",
            data: JSON.stringify(voteData),
            contentType: "application/json",
            dataType: "json"
        });
    }

    try {
        let response = await getTokenPopup(loginRequest);
        callMSGraph(graphConfig.graphMeEndpoint, response.accessToken, sendVote);
        const voteButton = Array.from(form.children).filter(e => e.classList.contains('cast-vote'))[0].firstElementChild;
        voteButton.innerHTML = "Vote Cast!";
        voteButton.onclick = () => {
            voteButton.innerHTML = "Vote Already Cast!";
            return false;
        };
        $(form).submit(e => e.preventDefault());
    } catch (err) {
        console.error(err);
    }
}