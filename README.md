# CS630 ACM Voting System

This project is designed to be deployed via AWS for a simple voting system, themed with UWRF branding.

## How to Deploy

All lines with a black triangle can be clicked to display a screenshot relevant to the step.

### Create a Project Execution Role

1. Go to the [IAM dashboard](https://console.aws.amazon.com/iam/home).
2. <details><summary>Go to the "Roles" Section</summary>
   
   ![IAM Roles Section](readme_files/roles_button.png)
   
   </details>
   
3. <details><summary>Press "Create role"</summary>
   
   ![Create Role Button](readme_files/create_role_button.png)
   
   </details>
   
4. <details><summary>For the role type, choose "AWS service"</summary>
   
   ![Role Type Selection](readme_files/role_type.png)
   
   </details>
   
5. <details><summary>For the use case, choose "Lambda." Keep mind, this is just a jumping off point, we will be adding 
   more AWS services to this role.</summary>
   
   ![Lambda Service Button](readme_files/lambda_service_button.png)
   
   </details>
   
6. Hit next. Search up and attach the following policies:
   - AmazonRDSFullAccess
   - AmazonEC2FullAccess
   - AmazonAPIGatewayPushToCloudWatchLogs
   - CloudWatchLogsFullAccess
   - AmazonAPIGatewayAdministrator
   - AmazonRDSDataFullAccess
   - AWSLambda_FullAccess
   
7. Skip the tags section, unless you need to add one for your specific implementation.

8. <details><summary>Continue to the review form. Give your role a recognizable name and press "Create role."</summary>
   
   ![Create Role Form](readme_files/create_role_form.png)
   
   </details>
   
9. We will be using this execution role across many of our services, so remember it!

### Create a MySQL RDS

##### Go to the [Amazon RDS dashboard](https://console.aws.amazon.com/rds/home) and press "Create new database."

1. Select the "Easy Create" database creation method.
2. Set engine type to MySQL.
3. Select the free tier DB size.
4. For the DB instance identifier, use an easy to remember dtabase name.
5. Master username can be whatever you desire.
6. I suggest leaving the password to auto-generate.
7. Press the "Create Database" button.
8. Make sure to write down the generated password, as you cannot retrieve this later.

<details><summary>Configuration Screenshot</summary>

![Create Database Image](readme_files/create_db.png)

</details>

##### Configure your database for local maintenance

We want to be able to work directly with our RDS, we need to open up the database's security group to the outside internet.

1. Click into our freshly made database after its status has changed to "available."
2. <details><summary>Under "Connectivity & security," Click into the VPC security group on the rightmost column.</summary>
   
   ![Security Group Location](readme_files/security_group_location.png)
   
   </details>
   
3. <details><summary>Scroll down to the "Inbound Rules" section and press "edit inbound rules."</summary>
   
   ![Inbound Rules Location](readme_files/security_group_inbound_rules_location.png)
   
   </details>
   
4. <details><summary>Press the "Add rule" button.</summary>
   
   ![Add Rule Button Location](readme_files/add_inbound_rule.png)
   
   </details>
   
5. <details><summary>Set the type to "MYSQL/Aurora," the protocol to "TCP," the port range to 3306, the source to "Custom," 
   the CIDR address to your [public IP address](https://www.whatsmyip.org/) with "/32" at the end, and the Description to 
   something like "Personal DB Access."</summary>
   
   ![New Inbound Rule](readme_files/new_inbound_rule.png)
   
   </details>
   
6. Return to the [Amazon RDS dashboard](https://console.aws.amazon.com/rds/home) and enter your database.
7. <details><summary>Press the "Modify" button in the upper right.</summary>
   
   ![Modify Button](readme_files/modify_button.png)
   
   </details>
   
8. Move down to the "Connectivity" section.
9. Press the "Additional configuration" drop down.
0. <details><summary>Set "Public access" to "Publicly accessible."</summary>
    
    ![Public Accessibility Button](readme_files/public_accessibility_button.png)
    
   </details>
   
1. Remember, remove the security group rule and disable public access after you finish administering the database!
   
##### Connect to the database and create the necessary tables

1. Download [MySQL Workbench](https://www.mysql.com/products/workbench/), or an alternative MySQL database administrator tool.
2. <details><summary>Write down your database endpoint and port. These can be found under the "Connectivity & security" tab under your database.</summary>
   
   ![Endpoint and Port Location](readme_files/endpoint_and_port.png)
   
   </details>
   
3. Open MySQL Workbench.
4. Press `Database > Connect to Database...` under the top bar
5. <details><summary>Enter our endpoint under "Hostname," our port under "port," our username under "username," and for
   the password we remember from our initial set up press "Store in Vault..." and paste it in the text box.</summary>
   
   ![Connect to Database Form](readme_files/connect_to_database.png)
   
   </details>
   
6. At this point, you should be able to connect to our database. If not, you may have missed a step from above.
7. First, lets take a note of the pre-existing databases. You can do so by running the following SQL query:

   ```sql
   SHOW DATABASES;
   ```
   
8. This should provide an output like the following:

   | Database |
   |----------|
   | information_schema |
   | mysql |
   | performance_schema |
   
9. These are the prepackaged databases for internal function of the database. _Do not touch them._
0. Let us now create a database to house our data. This can be down with the following SQL:

   ```sql
   CREATE DATABASE voting_system; /* Creates the database */
   USE voting_system;             /* Sets the database to the current scope */
   ```
   
1. Use the following queries to create our needed tables:

   ```sql
   CREATE TABLE elections ( 
        election_id INT NOT NULL AUTO_INCREMENT, 
        election_date TIMESTAMP NOT NULL, 
        election_name VARCHAR(255) NOT NULL, 
        PRIMARY KEY (election_id) 
    ); 
    
   CREATE TABLE positions ( 
        position_id INT NOT NULL AUTO_INCREMENT, 
        position_name VARCHAR(255) NOT NULL UNIQUE, 
        PRIMARY KEY (position_id) 
   ); 
    
   CREATE TABLE electors ( 
        elector_id INT NOT NULL AUTO_INCREMENT, 
        elector_name VARCHAR(255) NOT NULL UNIQUE, 
        PRIMARY KEY (elector_id) 
   ); 
    
   CREATE TABLE participants ( 
        participation_id INT NOT NULL AUTO_INCREMENT, 
        election_id INT NOT NULL, 
        position_id INT NOT NULL, 
        elector_id INT NOT NULL, 
        speech MEDIUMTEXT, 
        PRIMARY KEY (participation_id), 
        FOREIGN KEY (election_id) REFERENCES elections(election_id), 
        FOREIGN KEY (position_id) REFERENCES positions(position_id), 
        FOREIGN KEY (elector_id) REFERENCES electors(elector_id) 
   ); 
    
   CREATE TABLE votes ( 
        voter_id VARCHAR(100) NOT NULL, 
        participation_id INT NOT NULL, 
        vote_date DATETIME NOT NULL DEFAULT NOW(), 
        PRIMARY KEY (voter_id, participation_id), 
        FOREIGN KEY (participation_id) REFERENCES participants(participation_id) 
   );
   ```
   
2. Theoretically, that should be that is necessary for administering our database for now. You can now close the 
   previously added security hole.
   
##### Modify files for database access

If we want our code to be able to modify our database, it has to have our username and password.

<details><summary>Go to the [sql_functions.js](lambda_layer/nodejs/sql_functions.js) file and update sql connection 
object to have the correct username, password, database, and endpoint (if you used a port other than 3306, add a port property).</summary>

```js
const con = mysql.createConnection({
    host: "xxxxxxxxxx.xxxxxxxxxxxx.us-east-2.rds.amazonaws.com",
    user: "admin",
    password: "yyyyyyyyyyyy",
    database: "voting_system"
});
```

</details>
   
### Create a Lambda Layer For SQL Query Functionality

A lambda layer is a collection of shared files you can use across all lambda functions you set to implement the layer.

_Keep in mind, you cannot use this to share data between lambdas, as lambdas are stateless. You need some form of 
persistent storage in order to do this._

1. <details><summary>Go to the lambda layers dashboard from the [main lambda dashboard](https://console.aws.amazon.com/lambda/home).</summary>
   
   ![Lambda Layers Dashboard Button](readme_files/lambda_layer_dashboard.png)
   
   </details>
   
2. <details><summary>Press the "Create layer" button in the upper right of the dashboard</summary>
   
   ![Create Layer Button](readme_files/create_layer_button.png)
   
   </details>
   
3. <details><summary>Give the layer an applicable name, such as "UWRF-SQL," and upload a zip file of the 
   [nodejs directory](lambda_layer/nodejs) under the [lambda_layer directory](lambda_layer) with the same directory 
   structure and name as the example zip file included in the project, and set the Compatible runtime to "Node.js 12.x."</summary>
   
   ![Create Layer Form](readme_files/create_layer_form.png)
   
   </details>
   
   _Note: Do not use the included zip file, as this does not include your database's access credentials._
   
### Create Necessary Lambda Functions

##### Common Lambda set up

1. Set the runtime to Node.js 12.x. 
2. <details><summary>Open up the "Change default execution role" fold, set "Execution role" to "Use existing role," and 
   set "Existing role" to The role we made in the beginning of the lab.</summary>
   
   ![Set Execution Role Form](readme_files/lambda_set_role_form.png)
   
   </details>

3. <details><summary>Open up the "Advanced settings" fold. From the drop down under "VPC," choose the VPC used by 
   our database, use all of its subnets, and use its security group.</summary>
   
   ![Lambda VPC Form](readme_files/lambda_vpc_form.png)
   
   </details>
   
4. <details><summary>Once you are in the main page of your lambda function, click the "Layers" button.</summary>
   
   ![Layer Button](readme_files/layer_button.png)
   
   </details>
   
5. <details><summary>In the new "Layers" panel, press the "Add a layer" button.</summary>
   
   ![Add A Layer Button](readme_files/add_layer_button.png)
   
   </details>
   
6. <details><summary>Select "Custom layers," then from the first drop down select our layer. For the version, just 
   choose the latest which will probably just be 1.</summary>
   
   ![Layer Form](readme_files/layer_form.png)
   
   </details>

##### Individual lambda functions

Perform the above steps for each of the following code blocks. Paste these into the index.js file in the lambda function.

1. <details><summary>Add Election Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'election_name', 'election_date');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.addElection(event.election_name, new Date(event.election_date));
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
2. <details><summary>Remove Election Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'election_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.removeElection(event.election_name);
       console.log(result);
       return result.affectedRows != 0;
   };
   ```
   
   </details>

3. <details><summary>Add Elector Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'elector_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.addElector(event.elector_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
4. <details><summary>Remove Elector Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'elector_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.removeElector(event.elector_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
5. <details><summary>Add Position Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'position_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.addPosition(event.position_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
6. <details><summary>Remove Position Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'position_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.removePosition(event.position_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
7. <details><summary>Participate In Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'election_name', 'position_name', 'elector_name', 'speech');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.participateIn(event.election_name, event.position_name, event.elector_name, event.speech);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
8. <details><summary>Cast Vote Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'voter', 'position_name', 'elector_name') && sql_functions.validate(event.voter, 'id');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.castVote(event.voter.id, event.position_name, event.elector_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
9. <details><summary>Get Current Election Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let result = await sql_functions.getCurrentElection();
       let retVal = {};
       for (let i of result) {
           if (!retVal[i.position_name.substr(1, i.position_name.length - 2)]) retVal[i.position_name.substr(1, i.position_name.length - 2)] = [];
           retVal[i.position_name.substr(1, i.position_name.length - 2)].push({
               name: i.elector_name.substr(1, i.elector_name.length - 2),
               speech: i.speech.substr(1, i.speech.length - 2)
           });
       }
       return retVal;
   };
   ```
   
   </details>
   
0. <details><summary>Add Admin Lambda</summary>
   
   ```js
   const sql_functions = require('/opt/nodejs/sql_functions');
   
   exports.handler = async (event) => {
       let invalid = sql_functions.validate(event, 'authorizing_admin_id', 'new_admin_id', 'new_admin_name');
       if (invalid) return {
           statusCode: 400,
           body: invalid
       }
       let result = await sql_functions.addAdmin(event.authorizing_admin_id, event.new_admin_id, event.new_admin_name);
       return result.affectedRows != 0;
   };
   ```
   
   </details>
   
### Create an API Gateway

##### Create the gateway

1. <details><summary>Go to the [API Gateway dashboard](https://console.aws.amazon.com/apigateway/main/apis) and press the 
   "Create API" button in the upper right.</summary>
   
   ![Create API Button](readme_files/create_api_button.png)
   
   </details>
   
2. <details><summary>Choose "REST API" from the list and press "Build."</summary>
   
   ![REST API Build Button](readme_files/rest_api_build_button.PNG)
   
   </details>

3. Leave the API creation method as the default, "New API."

4. <details><summary>For the settings, give it a descriptive name, and the other options can be left as their defaults.</summary>
   
   ![REST API Settings Form](readme_files/rest_api_settings_form.png)
   
   </details>
   

##### Create a resource for each lambda function

Now that we have our API, we can create a resource for each of our lambda functions.

1. <details><summary>Make sure your scope is the root element by clicking on it.</summary>
   
   ![Root Resource](readme_files/root_resource.png)
   
   </details>
   
2. <details><summary>Press "Create Resource" under "Actions" from the root resource.</summary>
   
   ![Create Resource](readme_files/create_resource.png)
   
   </details>
   
3. <details><summary>In this form, give the resource a name, preferably correlating to its respective lambda 
   function, and be sure to enable CORS.</summary>
   
   ![Create Child Resource Form](readme_files/child_resource_form.png)
   
   </details>
  
4. Press "Create Resource."
5. Move our scope to our new resource by left clicking on it.
6. <details><summary>From the "Actions" dropdown, select "Create Method."</summary>
   
   ![Create Method](readme_files/create_method.png)
   
   </details>
  
7. From the dropdown, choose "Post."
8. <details><summary>In the POST setup page, keep the integration type as "Lambda function," confirm its in the 
   right region for our lambda functions, and type in the name of the given lambda function in the space provided.</summary>
   
   ![POST Setup](readme_files/post_setup.png)
   
   </details>
  
9. <details><summary>Repeat for every lambda function. At the end, it should look something like this.</summary>
   
   ![All Gateways](readme_files/all_gateways.png)
   
   </details>
   
##### Deploy the gateway

1. Choose "Deploy API" from the "Actions" dropdown.
2. Create a version and give it a name. 
3. On the next screen, copy the gateway URL in the blue banner.
4. In the project, open the [apiURL.js](website/js/apiURL.js). file and change the value of the `API_URL` constant to our gateway URL. 
   
### Create a Microsoft OAuth App Registration

##### Create the registration

1. Go to the [Azure Portal dashboard](https://portal.azure.com/#home).
2. Be sure to long in with your school email, otherwise we cannot require users to log in with their school accounts.
3. <details><summary>Once on the dashboard, click [the Azure Active Directory icon](https://portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/Overview).</summary>
   
   ![Azure Active Directory Button](readme_files/azure_active_directory_button.png)
   
   </details>
   
4. <details><summary>Now in the Azure Active Directory dashboard, press the "App registration" button in the "Create" section.</summary>
   
   ![App Registration Button](readme_files/app_registration_button.png)
   
   </details>
   
5. <details><summary>Give our application a reliavent name, and leave the rest their defaults. Press the blue "Register" on the bottom left.</summary>
   
   ![Register App Form](readme_files/register_app_form.png)
   
   </details>
   
##### Configuring our web app

We need 2 pieces of information from our registration in order to customize our web app, that being our client ID and authority.

<details><summary>These can both be acquired here on the within the app registration page.</summary>

![App Registration Values](readme_files/app_registration_values.png)

</details>

<details><summary>Find and replace these values in [website/oauth/authConfig.js](website/oauth/authConfig.js).</summary>

```js
const msalConfig = {
    auth: {
        clientId: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", /* <-- Here */
        authority: "https://login.microsoftonline.com/yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy", /* <-- And Here */
        redirectUri: `${window.location.href}`,
    },
    cache: {
        cacheLocation: "sessionStorage", // This configures where your cache will be stored
        storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
    },
    system: {	
        loggerOptions: {	
            loggerCallback: (level, message, containsPii) => {	
                if (containsPii) {		
                    return;		
                }		
                switch (level) {		
                    case msal.LogLevel.Error:		
                        console.error(message);		
                        return;		
                    case msal.LogLevel.Info:		
                        console.info(message);		
                        return;		
                    case msal.LogLevel.Verbose:		
                        console.debug(message);		
                        return;		
                    case msal.LogLevel.Warning:		
                        console.warn(message);		
                        return;		
                }	
            }	
        }	
    }
};
```

</details>
   
We will be returning to specify a domain name for our web app, so don't close this tab quite yet.
   
### Create a Static Website on AWS S3

##### Create the bucket

1. Go to the [Amazon S3 dashboard](https://s3.console.aws.amazon.com/s3/home).
2. <details><summary>Press the "Create bucket" button in the upper right.</summary>
   
   ![Create Bucket Button](readme_files/create_bucket.png)
   
   </details>
   
3. <details><summary>Give your bucket a memorable name and give it the same region as our lambda functions.</summary>
   
   ![Bucket Name Form](readme_files/bucket_name_form.png)
   
   </details>
   
4. <details><summary>Enable public access to our bucket.</summary>
   
   ![Bucket Public Access](readme_files/public_bucket_access.png)
   
   </details>
   
5. Leave the remaining settings as their defaults, and press the orange "Create bucket" button.

##### Configure bucket as a static website

1. Click into our new bucket.
2. Switch to the "Properties" tab.
3. <details><summary>Scroll down to the "Static website hosting" section, and press the "Edit" button.</summary>
   
   ![Edit Button](readme_files/static_website_edit_button.png)
   
   </details>
   
4. <details><summary>Enable static website hosting, set "Hosting type" to "Host a static website," set the index 
   document to "index.html," and the error document as "error.html."</summary>
   
   ![Static Website Form](readme_files/static_website_form.png)
   
   </details>
   
5. If we go back to the static website hosting section now, there will now be a "Bucket website endpoint" property. Write this down.   
6. Press the orange "Save changes" button.
7. Switch to the "Permissions" tab. 
8. Scroll down to the Access control list section, and press "Edit."
9. <details><summary>Give list and read access to all users.</summary>
   
   ![List and Read Access](readme_files/list_and_read_access.png)
   
   </details>

##### Upload website files 
 
1. Switch back to the "Objects" tab.
2. <details><summary>Press the orange "Upload" button.</summary>
   
   ![Upload Button](readme_files/bucket_upload_button.png)
   
   </details>
   
3. Upload all the files and folders inside of [the website directory](website) except for ".idea" and ".gitignore."
4. <details><summary>At this point your bucket contents should look something like _this_.</summary>
   
   ![Bucket Contents](readme_files/bucket_contents.png)
   
   </details>
   
5. <details><summary>Hit the uppermost checkbox on the left side in order to select all bucket objects.</summary>
   
   ![Bucket Checkbox](readme_files/bucket_checkbox.png)
   
   </details>
   
6. <details><summary>Hit the "Actions" dropdown, scroll down, and press "Make public".</summary>
   
   ![Make Public Button](readme_files/make_public_button.png)
   
   </details>
   
7. Press the orange "Make public" button.

8. We should now be able to visit the web address we wrote down earlier, though the "sign in" button will still not work. 
   This is because Microsoft OAuth requires an https connection.
   
### Create an AWS CloudFront Cache Service

1. Go to the [CloudFront dashboard](https://console.aws.amazon.com/cloudfront/home).
2. <details><summary>Press the blue "Create Distribution" button.</summary>
   
   ![Create Distribution Button](readme_files/create_distrobution_button.png)
   
   </details>
   
3. Select the web delivery method.
4. <details><summary>Under the "Origin Settings" section, set "Origin Domain Name" to the endpoint of our S3 bucket.</summary>
   
   ![Origin Domain Name](readme_files/origin_domain_input.png)
   
   </details>
   
5. <details><summary>Under the "Default Cache Behavior Settings" section, set "Viewer Protocol Policy" to "Redirect HTTP 
   to HTTPS."</summary>
   
   ![Viewer Protocol Policy](readme_files/viewer_protocol_policy.png)
   
   </details>
   
6. <details><summary>Under the "Distribution Settings" section, set "Price Class" to "Use Only U.S., Canada and Europe."</summary>
   
   ![Price Class](readme_files/price_class.png)
   
   </details>
   
7. <details><summary>Also under the "Distribution Settings" section, set "Default Root Object" to "index.html."</summary>
   
   ![Default Root Object](readme_files/default_root_object.png)
   
   </details>
   
8. Leave the remaining sections as their defaults, and hit the blue "Create Distribution" button in the bottom right of the form.
9. Once it has generated, click into our new CloudFront Distribution.
0. <details><summary>Under the distribution's general properties, we can find its domain name. Write this down for later.</summary>
   
   ![Domain Name Property](readme_files/domain_name.png)
   
   </details>
   
### Registering Our Web Application

Now we finally have a URL that our web app will be hosted on, we can specify it to our Microsoft OAuth app registration.

1. Go to the [Azure Portal dashboard](https://portal.azure.com/#home).
2. <details><summary>Once on the dashboard, click [the Azure Active Directory icon](https://portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/Overview).</summary>
   
   ![Azure Active Directory Button](readme_files/azure_active_directory_button.png)
   
   </details>
   
3. <details><summary>Click "App registrations" under the "Manage" section from the tool bar on the left.</summary>
   
   ![App Registration Button](readme_files/app_registration_button.png)
   
   </details>
   
4. Click on your app registration.
5. <details><summary>Click on the "Redirect URIs" link on the right.</summary>
   
   ![Redirect URIs Link](readme_files/redirect_uris.png)
   
   </details>
   
6. <details><summary>Press the blue plus with the text "Add a platform" at the top of the page.</summary>
   
   ![Add a Platform](readme_files/add_a_platform.png)
   
   </details>
   
7. <details><summary>Select "Single-page application" from the pane on the right.</summary>
   
   ![Single Page Application Option](readme_files/single_page_application_option.png)
   
   </details>
   
8. <details><summary>In the input field, paste in our cloudfront domain from before.</summary>
   
   ![SPA Domain Name](readme_files/spa_domain_name.png)
   
   </details>
   
9. Press the blue "Configure" button.
0. <details><summary>There is now a single page application redirect URI on our application! Now press the blue "Add URI" 
   link below our domain name twice and add separate entries for our index and admin html files. It should look like the 
   following.</summary>
   
   ![All URIs](readme_files/all_uris.png)
   
   </details>
   
1. Finally and most importantly, remember to press the blue "save" button in the upper left to commit our changes to the app registration.

##### And that's it!

Your deployment is now complete. You can now visit the CloudFront URL in a web browser, login with a university email 
address, and cast votes.

Alternatively, you can visit the admin page and create new elections.