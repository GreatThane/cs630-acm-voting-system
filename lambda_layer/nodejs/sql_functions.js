const mysql = require('mysql');
const sqlstring = require('sqlstring');

const con = mysql.createConnection({
    host: "xxxxxxxxxx.xxxxxxxxxxxx.us-east-2.rds.amazonaws.com",
    user: "admin",
    password: "yyyyyyyyyyyy",
    database: "voting_system"
});

async function query(queryString) {
    console.log("Query: \n" + queryString);
    return new Promise((resolve, reject) => {
        con.query(queryString, (err, result) => {
            if (err) {
                reject(err);
            } else resolve(result);
        });
    });
}

/**
 * Adds an election to the system.
 *
 * @param {string} electionName Desired name of a given election. Duplicates allowed.
 * @param {Date} dateTime Desired time for end of election.
 * @returns {Promise<Object>} Query results
 */
module.exports.addElection = async function (electionName, dateTime) {
    return query(`
    INSERT INTO elections (election_date, election_name) 
    SELECT TIMESTAMP("${dateTime.toISOString().split('T')[0]}", "${dateTime.toTimeString().split(' ')[0]}"), "${sqlstring.escape(electionName)}"
    WHERE (SELECT COUNT(*) FROM elections
        WHERE election_date > NOW()
        AND election_name = "${sqlstring.escape(electionName)}") = 0;`);
}

/**
 * Removes a given election. Prioritizes the nearest future election of the given name.
 *
 * @param {string} electionName Election desired to be removed.
 * @returns {Promise<Object>} Query results
 */
module.exports.removeElection = async function (electionName) {
    return query(`
    DELETE FROM elections 
    WHERE election_name="${sqlstring.escape(electionName)}" 
    AND election_date > NOW() 
    ORDER BY UNIX_TIMESTAMP(election_date) 
    LIMIT 1;`);
}

/**
 * Drops all tables and recreates them. Danger function.
 *
 * @returns {Promise<Object>} Query results
 */
module.exports.remakeTables = async function () {
    return query(`
    DROP TABLE votes; 
    DROP TABLE participants; 
    DROP TABLE elections; 
    DROP TABLE positions; 
    DROP TABLE electors; 
       
    CREATE TABLE elections ( 
        election_id INT NOT NULL AUTO_INCREMENT, 
        election_date TIMESTAMP NOT NULL, 
        election_name VARCHAR(255) NOT NULL, 
        PRIMARY KEY (election_id) 
    ); 
    
    CREATE TABLE positions ( 
        position_id INT NOT NULL AUTO_INCREMENT, 
        position_name VARCHAR(255) NOT NULL UNIQUE, 
        PRIMARY KEY (position_id) 
    ); 
    
    CREATE TABLE electors ( 
        elector_id INT NOT NULL AUTO_INCREMENT, 
        elector_name VARCHAR(255) NOT NULL UNIQUE, 
        PRIMARY KEY (elector_id) 
    ); 
    
    CREATE TABLE participants ( 
        participation_id INT NOT NULL AUTO_INCREMENT, 
        election_id INT NOT NULL, 
        position_id INT NOT NULL, 
        elector_id INT NOT NULL, 
        speech MEDIUMTEXT, 
        PRIMARY KEY (participation_id), 
        FOREIGN KEY (election_id) REFERENCES elections(election_id), 
        FOREIGN KEY (position_id) REFERENCES positions(position_id), 
        FOREIGN KEY (elector_id) REFERENCES electors(elector_id) 
    ); 
    
    CREATE TABLE votes ( 
        voter_id VARCHAR(100) NOT NULL, 
        participation_id INT NOT NULL, 
        vote_date DATETIME NOT NULL DEFAULT NOW(), 
        PRIMARY KEY (voter_id, participation_id), 
        FOREIGN KEY (participation_id) REFERENCES participants(participation_id) 
    );`);
}

/**
 * Add elector to the database to run in future elections.
 *
 * @param {string} electorName Name of elector to add. Must be unique.
 * @returns {Promise<Object>} Query results
 */
module.exports.addElector = async function (electorName) {
    return query(`
    INSERT INTO electors (elector_name) 
    SELECT "${sqlstring.escape(electorName)}" 
    WHERE (SELECT COUNT(elector_name) FROM electors 
        WHERE elector_name = "${sqlstring.escape(electorName)}") = 0;`);
}

/**
 * Remove elector from the database.
 *
 * @param {string} electorName Elector to be removed.
 * @returns {Promise<Object>} Query results
 */
module.exports.removeElector = async function (electorName) {
    return query(`DELETE FROM electors WHERE elector_name="${sqlstring.escape(electorName)}";`);
}

/**
 * Adds a position to the database to be run for in future elections.
 *
 * @param {string} positionName Position name. Must be unique.
 * @returns {Promise<Object>} Query results
 */
module.exports.addPosition = async function (positionName) {
    return query(`
    INSERT INTO positions (position_name) 
    SELECT "${sqlstring.escape(positionName)}" 
    WHERE (SELECT COUNT(position_name) FROM positions 
        WHERE position_name = "${sqlstring.escape(positionName)}") = 0;`);
}

/**
 * Removes a position from the database.
 *
 * @param {string} positionName Position name. Must be unique.
 * @returns {Promise<Object>} Query results
 */
module.exports.removePosition = async function (positionName) {
    return query(`DELETE FROM positions WHERE position_name="${sqlstring.escape(positionName)}";`);
}

/**
 * Assigns a given elector to a given election for a given position with a given speech.
 *
 * @param electionName {string}
 * @param positionName {string}
 * @param electorName {string}
 * @param speech {string}
 * @returns {Promise<Object>} Query result
 */
module.exports.participateIn = async function (electionName, positionName, electorName, speech) {
    return query(`
    INSERT INTO participants (election_id, position_id, elector_id, speech) SELECT 
        (SELECT election_id FROM elections 
            WHERE election_name="${sqlstring.escape(electionName)}" 
            AND election_date > NOW() 
            ORDER BY UNIX_TIMESTAMP(election_date) 
            LIMIT 1), 
        (SELECT position_id FROM positions 
            WHERE position_name="${sqlstring.escape(positionName)}" 
            LIMIT 1), 
        (SELECT elector_id FROM electors 
            WHERE elector_name="${sqlstring.escape(electorName)}" 
            LIMIT 1), 
        "${sqlstring.escape(speech)}" 
    WHERE (SELECT COUNT(*) FROM participants
        INNER JOIN elections on elections.election_id = participants.election_id
        INNER JOIN electors on electors.elector_id = participants.elector_id
        WHERE elections.election_name="${sqlstring.escape(electionName)}"
        AND electors.elector_name="${sqlstring.escape(electorName)}"
    )=0;`);
}

/**
 * Gets the election options for the next occurring election.
 *
 * @returns {Promise<Array>} Query results
 */
module.exports.getCurrentElection = async function () {
    return query(`
    SELECT election_name, election_date, position_name, elector_name, speech 
    FROM elections 
    INNER JOIN participants on elections.election_id = participants.election_id 
    INNER JOIN electors on electors.elector_id = participants.elector_id
    INNER JOIN positions on positions.position_id = participants.position_id
    WHERE election_date > NOW() 
    AND elections.election_id=( 
        SELECT election_id 
        FROM elections 
        WHERE election_date > NOW() 
        ORDER BY UNIX_TIMESTAMP(election_date) 
        LIMIT 1);`);
}

/**
 * Casts a vote for the given elector for the given position by the given voter.
 *
 * @param voterID {string}
 * @param positionName {string}
 * @param electorName {string}
 * @returns {Promise<Object>} Query results
 */
module.exports.castVote = async function (voterID, positionName, electorName) {
    return query(`
    INSERT INTO votes (voter_id, participation_id, vote_date) 
    SELECT 
        "${voterID}", 
        (SELECT participation_id FROM participants 
            WHERE election_id=( 
                SELECT election_id FROM elections 
                WHERE election_date > NOW() 
                ORDER BY UNIX_TIMESTAMP(election_date) 
                LIMIT 1) 
            AND position_id=( 
                SELECT position_id FROM positions 
                WHERE position_name="${sqlstring.escape(positionName)}" 
                LIMIT 1) 
            AND elector_id=( 
                SELECT elector_id FROM electors 
                WHERE elector_name="${sqlstring.escape(electorName)}" 
                LIMIT 1) 
            LIMIT 1), 
        NOW()
    WHERE ( 
        SELECT COUNT(*) FROM votes 
        INNER JOIN participants 
        WHERE position_id="${sqlstring.escape(positionName)}" 
    )=0; `);
}

/**
 * Allows an already existing admin to add another admin.
 *
 * @param authorizingAdminID {string}
 * @param newAdminID {string}
 * @param newAdminName {string}
 * @returns {Promise<Object>} Query results
 */
module.exports.addAdmin = async function (authorizingAdminID, newAdminID, newAdminName) {
    return query(`
    INSERT INTO admins 
    SELECT "${sqlstring.escape(newAdminID)}", "${sqlstring.escape(newAdminName)}" 
    WHERE (SELECT COUNT(admin_id) FROM admins 
        WHERE admin_id = "${sqlstring.escape(authorizingAdminID)}") > 0 
    AND (SELECT COUNT(admin_id) FROM admins 
        WHERE admin_id = "${sqlstring.escape(newAdminID)}") = 0;`);
}

/**
 * Ensures the given object has the given properties.
 *
 * @param event {Object}
 * @param vars {...string} properties of event
 * @returns {string | null}
 */
module.exports.validate = function (event, ...vars) {
    for (let i of vars) {
        if (!event.hasOwnProperty(i)) return `Missing ${i} property`;
    }
    return null;
}