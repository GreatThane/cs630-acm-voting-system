const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let result = await sql_functions.getCurrentElection();
    let retVal = {};
    for (let i of result) {
        if (!retVal[i.position_name.substr(1, i.position_name.length - 2)]) retVal[i.position_name.substr(1, i.position_name.length - 2)] = [];
        retVal[i.position_name.substr(1, i.position_name.length - 2)].push({
            name: i.elector_name.substr(1, i.elector_name.length - 2),
            speech: i.speech.substr(1, i.speech.length - 2)
        });
    }
    return retVal;
};