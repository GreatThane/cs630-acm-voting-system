const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'position_name');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.removePosition(event.position_name);
    return result.affectedRows != 0;
};