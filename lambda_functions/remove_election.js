const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'election_name');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.removeElection(event.election_name);
    console.log(result);
    return result.affectedRows != 0;
};
