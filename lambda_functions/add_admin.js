const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'authorizing_admin_id', 'new_admin_id', 'new_admin_name');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.addAdmin(event.authorizing_admin_id, event.new_admin_id, event.new_admin_name);
    return result.affectedRows != 0;
};