const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'voter', 'position_name', 'elector_name') && sql_functions.validate(event.voter, 'id');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.castVote(event.voter.id, event.position_name, event.elector_name);
    return result.affectedRows != 0;
};