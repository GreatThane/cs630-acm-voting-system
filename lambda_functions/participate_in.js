const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'election_name', 'position_name', 'elector_name', 'speech');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.participateIn(event.election_name, event.position_name, event.elector_name, event.speech);
    return result.affectedRows != 0;
};