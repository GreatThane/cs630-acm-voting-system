const sql_functions = require('/opt/nodejs/sql_functions');

exports.handler = async (event) => {
    let invalid = sql_functions.validate(event, 'election_name', 'election_date');
    if (invalid) return {
        statusCode: 400,
        body: invalid
    }
    let result = await sql_functions.addElection(event.election_name, new Date(event.election_date));
    return result.affectedRows != 0;
};